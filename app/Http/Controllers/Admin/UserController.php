<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\MediaController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Inertia\Inertia;

class UserController extends Controller
{

    public function index(Request $request)
    {
        $userSql = User::orderBy('id', 'desc');
        $key = '';
        if (isset($request->key)) {
            $userSql->where('name', 'LIKE', '%' . $request->key . '%');
            $userSql->orWhere('email', 'LIKE', '%' . $request->key . '%');
            $userSql->orWhere('id', $request->key);
            $key = $request->key;
        }

        $users = $userSql->paginate(25);
        return Inertia::render('users/Index', compact('users', 'key'));
    }

    public function create()
    {
        return Inertia::render('users/Create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => ['required', 'string', 'max:255'],
            'avatar'    => ['nullable', 'image'],
            'email'     => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password'  => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if ($request->hasFile('avatar')) {
            $file = (new MediaController())->imageUpload($request->file('avatar'), 'avatar', '');
            $avatar = $file['name'];
        }

        User::create([
            'name'     => $request->name,
            'email'    => $request->email,
            'avatar'    => isset($avatar) ? $avatar : null,
            'password' => bcrypt($request->password),
        ]);

        return redirect()->route('admin.users.index')->with('success', 'User create successfully');
    }

    public function edit($id)
    {
        $data = User::find($id);
        return Inertia::render('users/Edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'      => ['required', 'string', 'max:255'],
            'avatar'    => ['nullable', 'image'],
            'email'     => ['required', 'string', 'email', 'max:255',Rule::unique('users')->ignore($id) ],
        ]);

        $user = User::find($id);
        $avatar = $user->avatar;
        if ($request->hasFile('avatar')) {
            $media = new MediaController();
            $media->delete('avatar', $user->avatar, '');
            $file = $media->imageUpload($request->file('avatar'), 'avatar', '');
            $avatar = $file['name'];
        }

        $user->update([
            'name'     => $request->name,
            'email'    => $request->email,
            'avatar'    => $avatar,
            'password' => isset($request->password) ? bcrypt($request->password) : $user->password,
        ]);

        return redirect()->route('admin.users.index')->with('success', 'User update successfully');
    }


    public function destroy($id)
    {
        try {
            $user = User::find($id);
            (new MediaController())->delete('avatar', $user->avatar, '');
            $user->delete();

            return redirect()->route('admin.users.index')->with('success', 'User delete successfully');
        }catch (\Exception $exception) {
            return redirect()->route('admin.users.index')->with('error', $exception->getMessage());
        }
    }

    public function all(Request $request) {
        try {
            $userSql = User::orderBy('id', 'desc');
            $key = '';
            if (isset($request->key)) {
                $userSql->where('name', 'LIKE', '%' . $request->key . '%');
                $userSql->orWhere('email', 'LIKE', '%' . $request->key . '%');
                $userSql->orWhere('id', $request->key);
                $key = $request->key;
            }
            $users = $userSql->paginate(25);

            return response()->json([
                'status' => true,
                'message' => 'Data get successful.',
                'data' => $users
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ]);
        }


    }
}
