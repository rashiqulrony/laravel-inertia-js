<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

class WebController extends Controller
{
    public function index()
    {
        $users = [
            [
                'id' => 1,
                'name' => 'Rony',
            ],
            [
                'id' => 2,
                'name' => 'Atik Ulta',
            ],
            [
                'id' => 3,
                'name' => 'Sadhin',
            ],
        ];
        return Inertia::render('Home',[
            'users' => $users,
        ]);
    }

    public function dashboard()
    {
        return Inertia::render('Dashboard');
    }

    public function about()
    {
        $user = [
                'id' => 1,
                'name' => 'Rony',
            ];
        return Inertia::render('About',['user' => $user]);
    }

    public function details($id) {
        $user = [
            'id' => 1,
            'name' => 'Rony',
        ];
        return Inertia::render('About',['user' => $user]);
    }
}
