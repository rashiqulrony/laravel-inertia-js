const mix = require('laravel-mix');
const path = require('path');
const publicAssets = 'public/admin/';
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .vue()
    .webpackConfig({
        output: { chunkFilename: 'js/[name].js?id=[chunkhash]' },
        resolve: {
            alias: {
                '@': path.resolve('resources/js'),
                route: path.resolve('vendor/tightenco/ziggy/dist'),
            }
        }
    })
    .version()
    .sourceMaps();

mix.styles([
    publicAssets+'css/bootstrap.min.css',
    publicAssets+'assets/font-awesome/css/font-awesome.css',
    publicAssets+'assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css',
    publicAssets+'css/owl.carousel.css',
    publicAssets+'css/slidebars.css',
    publicAssets+'css/style.css',
    publicAssets+'css/style-responsive.css',
], publicAssets+'css/all.css');


//All js added
mix.scripts([
    publicAssets+'js/jquery.js',
    publicAssets+'js/bootstrap.bundle.min.js',
    publicAssets+'js/jquery.dcjqaccordion.2.7.js',
    publicAssets+'js/jquery.scrollTo.min.js',
    publicAssets+'js/jquery.nicescroll.js',
    publicAssets+'js/jquery.sparkline.js',
    publicAssets+'assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js',
    publicAssets+'js/owl.carousel.js',
    publicAssets+'js/jquery.customSelect.min.js',
    publicAssets+'js/respond.min.js',
    publicAssets+'js/slidebars.min.js',
    publicAssets+'js/common-scripts.js',
    publicAssets+'js/sparkline-chart.js',
    publicAssets+'js/easy-pie-chart.js',
    publicAssets+'js/count.js',
    publicAssets+'js/count.js',
    publicAssets+'js/custom.js',
], publicAssets+'js/all.js');

