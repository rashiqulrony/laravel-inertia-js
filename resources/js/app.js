import Vue     from 'vue';
import axios   from 'axios';
import { createInertiaApp, plugin } from '@inertiajs/inertia-vue';
import { InertiaProgress } from '@inertiajs/progress'
import Layout from './components/share/Layout';
import VueToastr2 from 'vue-toastr-2'
import 'vue-toastr-2/dist/vue-toastr-2.min.css'

import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueSweetalert2);

window.toastr = require('toastr');
Vue.use(VueToastr2);

InertiaProgress.init();
Vue.use(plugin);
window.axios = axios;

const el = document.getElementById('app');
createInertiaApp({
    title: title => `RonApp | ${title}`,
    resolve: name => {
        const page = require(`./components/${name}`).default;
        page.layout = page.layout || Layout;
        return page
    },
    setup({ el, App, props }) {
        Vue.mixin({
            methods: {
                route,
            },
        });
        new Vue({
            render: h => h(App, props),
        }).$mount(el)
    },

})

/*new Vue({
    render: h => h(App, {
        props: {
            initialPage: JSON.parse(el.dataset.page),
            resolveComponent: name => import('@/components/' + name + '.vue').then(module => module.default)
        }
    })
}).$mount(el)*/
