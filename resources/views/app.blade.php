<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Rashiqul Rony">
    <meta name="keyword" content="inertia, app, rashiqul rony">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('admin') }}/img/favicon.html">

    <link href="{{ mix('/admin/css/all.css') }}" rel="stylesheet"></link>
    @routes
</head>

<body class="light-sidebar-nav">
@inertia
<script src="{{ mix('/admin/js/all.js') }}"></script>
<script src="{{ mix('/js/app.js') }}"></script>

</body>
</html>
