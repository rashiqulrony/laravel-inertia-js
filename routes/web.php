<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'admin'], function () {
    Auth::routes();
});


Route::get('/', function () {
    return redirect('admin/login');
});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth:web']], function () {
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('users/all', 'UserController@all')->name('users.all');
    Route::resource('users', 'UserController');
});



